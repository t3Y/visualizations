#!/usr/bin/env python3

#  logistic map bifurcation graph
#  Copyright (C) 2020 contact@t3Y.eu
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


import matplotlib.pyplot as plt
import argparse
import logging
import os
from random import random
from math import log10


"""
    bifurcation graph of the logistic function
    precision of 960 gives 4k resolution frames
"""


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--iterations", type=int, default=10, help="how many iterations should we run for each r")
    parser.add_argument("-p", "--precision", type=int, default=100, help="how many values of r do we try; the actual number will be 5 times what you provided")
    parser.add_argument("-m", "--min", type=float, default=0.0, help="only consider values of r greater or equal to min")
    parser.add_argument("-s", "--skip", type=int, default=None, help="how many values of the series to skip before we graph, defaults to a fifth of the series")
    parser.add_argument("-a", "--animation", help="output unlabeled, margin-less frames for an animation instead of a single graph", action="store_true")
    parser.add_argument("-v", "--verbose", help="if provided, display info messages", action="store_true")
    args = parser.parse_args()
    return args


def logistic_map(r, x):
    return r * x * (1-x)


def calculate_series(iterations, r, start):
    series = [0] * iterations
    x = start
    series[0] = x
    for i in range(1, iterations):
        x = logistic_map(r, x)
        series[i] = x
    return series


def plot(series_list, r_values, ignore, iterations, animation=False):

    fig, axis = plt.subplots(1)
    for i in range(len(series_list)):
        series = series_list[i]
        r = [r_values[i]] * len(series[ignore:iterations])
        axis.plot(r, series[ignore:iterations], ",k")
    logging.info("formatting the figure")
    width = max(8, (len(series_list) / 100))
    height = width / 1.777777777
    axis.set_xlabel("r")
    axis.set_ylabel("x")
    fig.set_size_inches(width, height)
    filename = str(iterations).zfill(1 + int(log10(len(series_list[0])))) + ".png"
    if animation:
        axis.get_xaxis().set_visible(False)
        axis.get_yaxis().set_visible(False)
        axis.margins(0, 0)
        fig.text(0.06, 0.93, "n={}".format(iterations), bbox=dict(facecolor='none'), fontsize=width)
        plt.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
    else:
        # fig.text(0.15, 0.90, "n={}".format(iterations), bbox=dict(facecolor='none'))
        fig.set_tight_layout(True)

    logging.info("writing {}x{} figure to {}".format(int(width*100), int(height*100), filename))
    fig.savefig(os.path.join("output", filename))
    fig.clear()
    plt.close(fig)


def check_args(args):

    iterations = 10
    if args.iterations > 0:
        iterations = args.iterations
    else:
        logging.warning("invalid number of iterations:{}, using default of {}".format(args.iterations, iterations))

    ignore = None
    if args.skip is not None:
        if args.skip > -1 and args.skip < iterations:
            ignore = args.skip
        else:
            logging.warning("invalid number of ignore steps:{}, using default of min(12, n/5)".format(args.skip))

    step = 0.1
    if args.precision > 0:
        step = 1 / args.precision
    else:
        logging.warning("invalid precision level:{}, using default of {}".format(args.precision, 1 / step))

    return (iterations, ignore, step)


def main():

    args = parse_args()
    log_level = logging.WARNING
    if args.verbose:
        log_level = logging.INFO
    logging.basicConfig(level=log_level, format="%(asctime)s %(levelname)s: %(message)s", datefmt="%Y-%m-%d %H:%M:%S")

    if not os.path.exists("output"):
        logging.info("creating output directory")
        os.makedirs("output")

    series_list = []
    r_values = []
    r = args.min

    max_iterations, usr_provided_ignore, step = check_args(args)

    min_iterations = max_iterations
    iterations = max_iterations
    max_iterations += 1
    if args.animation:
        min_iterations = 1

    logging.info("generating data")
    while r <= 4.0:
        start = random()
        while start == 0:
            start = random()
        series_list.append(calculate_series(iterations, r, start))
        r_values.append(r)

        r += step

    for iterations in range(min_iterations, max_iterations):

        if args.animation and usr_provided_ignore is not None:
            ignore = usr_provided_ignore
        else:
            ignore = min(12, iterations // 5)

        logging.info("generating plot for n={}".format(iterations))
        plot(series_list, r_values, ignore, iterations, args.animation)
    logging.info("done")


main()
